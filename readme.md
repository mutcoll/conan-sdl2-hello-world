# Hello World for SDL2 conan package

This repo is intented to test that the conan package SDL2/2.0.1@lasote/stable
works fine, and to test changing options, such as `shared = True/False`, in linux and windows.

## Using
To test this make sure you have the dependencies, and use this commands:

### Linux
```
git clone https://bitbucket.org/mutcoll/conan-sdl2-hello-world.git
cd conan-sdl2-hello-world
conan install --build=missing
cmake -G "Unix Makefiles"
make
./bin/test_sdl2
```

### Windows

```
git clone https://bitbucket.org/mutcoll/conan-sdl2-hello-world.git
cd conan-sdl2-hello-world
conan install --build=missing
cmake -G "Visual Studio 14 Win64"
cmake --build .
./bin/test_sdl2.exe
```

## Dependencies

- C++ compiler (`sudo apt-get install build-essential`)
- cmake (`sudo apt-get install cmake`)
- conan (download [here](https://www.conan.io/downloads), recommended: `sudo apt-get install python-pip; sudo pip install conan`)
